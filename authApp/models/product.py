from django.db import models

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.FloatField(blank=True, null=True)
    description = models.CharField(max_length=500)
    size =  models.CharField(max_length=2)
    imageURL = models.CharField(max_length=500)
    isActive = models.BooleanField(default=True)
