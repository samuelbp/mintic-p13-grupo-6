import json
import jwt
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

# Create your tests here.

class TestAPI(TestCase):
    def test_signUp(self):
        client = APIClient()
        response = client.post(
            '/user',
            {
            "username": "user_prueba_1",
            "password": "password_prueba_1",
            "name": "user prueba",
            "email": "user_prueba_1@misionTIC.com",
            "account": { 
                "lastChangeDate": "2021-09-23T10:25:43.511Z",
                "balance": 20000,
                "isActive": "true"
                }
            },
        format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual('refresh' in response.data.keys(), True)
        self.assertEqual('access' in response.data.keys(), True)

    
    def test_login(self):
        client = APIClient()
        response = client.post(
            '/login',
            {
                "username":"samuelbp",
                "password":""
            },
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual('refresh' in response.data.keys(), True)
        self.assertEqual('access' in response.data.keys(), True)


    def test_user(self):
            client = APIClient()
            response = client.post(
                '/login',
                {
                    "username":"samuelbp",
                    "password":""
                },
                format='json')
            
            token_access =  response.data['access']
            # SECRET_KEY se toma de settings.py
            secret= "django-insecure-r0qmk$gg$oc&$%8%q9dt3&xkfoor8o92otpq5sm25=+g7v1)en"
            user_id = jwt.decode(token_access, secret, algorithms=["HS256"])["user_id"]
            url= '/user/'+str(user_id)
            auth_headers = {'HTTP_AUTHORIZATION': 'Bearer ' +token_access}

            res = client.get(url, **auth_headers)
            self.assertEqual(res.status_code, status.HTTP_200_OK)
            self.assertEqual(res.data["username"], "samuelbp")